from database_connection import Connection
from plot import Architecture
from plot import Network
from plot import Attribute


class DiagramGenerator(object):

    def __init__(self, db_connection, db_meta):
        self._nodes = db_meta.nodes
        self._edges = db_meta.edges
        self._db_connection = db_connection

    def _connect(self):
        pass

    def _find_nodes(self):
        pass

    def _find_edges(self):
        pass

    def generate_diagram(self):
        try:
            self._connect()
        except Exception as e:
            print ('Could not connect: {0}'.format(e.message))

        nodes = self._find_nodes()
        edges = self._find_edges()

