Twine
=====

Twine is a network graph model generation library. Simply define what nodes and edges are, point it at a database and it will generate a dot diagram (.diag) file.
